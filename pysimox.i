%module pysimox

/**************************************
 ***         Simox headers          ***
 **************************************/

%{
        #include "VirtualRobot/Robot.h"
		#include "VirtualRobot/Import/SimoxXMLFactory.h"
        #include "VirtualRobot/Nodes/PositionSensor.h"
        #include "VirtualRobot/Nodes/RobotNodePrismatic.h"
        #include "VirtualRobot/Nodes/RobotNodeRevolute.h"
        #include "VirtualRobot/SceneObjectSet.h"
        #include "VirtualRobot/RobotNodeSet.h"
        #include "VirtualRobot/EndEffector/EndEffector.h"
        #include "VirtualRobot/IK/DifferentialIK.h"
        #include "VirtualRobot/IK/JacobiProvider.h"
        #include "VirtualRobot/IK/IKSolver.h"
	#include "VirtualRobot/IK/SupportPolygon.h"
	#include "VirtualRobot/MathTools.h"
	#include "VirtualRobot/CollisionDetection/CollisionChecker.h"
	#include "VirtualRobot/CollisionDetection/CollisionModel.h"
%}

/**************************************
 ***     STL / Boost components     ***
 **************************************/

%include "boost_shared_ptr.i"
%shared_ptr(VirtualRobot::LocalRobot)
%shared_ptr(VirtualRobot::PositionSensor)
%shared_ptr(VirtualRobot::Robot)
%shared_ptr(VirtualRobot::RobotImporterFactory)
%shared_ptr(VirtualRobot::SceneObject)
%shared_ptr(VirtualRobot::RobotNode)
%shared_ptr(VirtualRobot::RobotNodePrismatic)
%shared_ptr(VirtualRobot::RobotNodeRevolute)
%shared_ptr(VirtualRobot::RobotNodeSet)
%shared_ptr(VirtualRobot::Sensor)
%shared_ptr(VirtualRobot::SimoxXMLFactory)
%shared_ptr(VirtualRobot::DifferentialIK)
%shared_ptr(VirtualRobot::EndEffector)
%shared_ptr(VirtualRobot::SupportPolygon)
%shared_ptr(VirtualRobot::CollisionModel)

%include "std_string.i"
%include "std_vector.i"

namespace std {
    %template(StdVectorFloat) vector<float>;
    %template(StdVectorString) vector<std::string>;
}

%template(SensorPtr) boost::shared_ptr<VirtualRobot::Sensor>;
%template(StdVectorSensorPtr) std::vector<boost::shared_ptr<VirtualRobot::Sensor> >;

%template(SceneObjectPtr) boost::shared_ptr<VirtualRobot::SceneObject>;
%template(StdVectorSceneObjectPtr) std::vector<boost::shared_ptr<VirtualRobot::SceneObject> >;

%template(SceneObjectSetPtr) boost::shared_ptr<VirtualRobot::SceneObjectSet>;
%template(StdVectorSceneObjectSetPtr) std::vector<boost::shared_ptr<VirtualRobot::SceneObjectSet> >;

%template(RobotNodePtr) boost::shared_ptr<VirtualRobot::RobotNode>;
%template(StdVectorRobotNodePtr) std::vector<boost::shared_ptr<VirtualRobot::RobotNode> >;

%template(RobotNodeSetPtr) boost::shared_ptr<VirtualRobot::RobotNodeSet>;
%template(StdVectorRobotNodeSetPtr) std::vector<boost::shared_ptr<VirtualRobot::RobotNodeSet> >;

%template(EndEffectorPtr) boost::shared_ptr<VirtualRobot::EndEffector>;
%template(StdVectorEndEffectorPtr) std::vector<boost::shared_ptr<VirtualRobot::EndEffector> >;

/**************************************
 ***       Eigen 3 interface        ***
 **************************************/

// Largely from https://bitbucket.org/MartinFelis/eigen3swig/

%include <eigen.i>

%eigen_typemaps(Eigen::Vector3f)
%eigen_typemaps(Eigen::Matrix3f)
%eigen_typemaps(Eigen::Matrix4f)
%eigen_typemaps(Eigen::MatrixXf)

// Rename all methods called "print" to "print_instance" because print is a reserved keyword in Python
%rename(print_instance) print;

namespace VirtualRobot {
	/**************************************
	 ***      Forward Declarations      ***
	 **************************************/

	class BoundingBox;

	class CollisionChecker;
	typedef boost::shared_ptr<CollisionChecker> CollisionCheckerPtr;

	class CollisionModel;
	typedef boost::shared_ptr<CollisionModel> CollisionModelPtr;

	class DHParameter;

	class LocalRobot;
	typedef boost::shared_ptr<LocalRobot> LocalRobotPtr;

	class Robot;
	typedef boost::shared_ptr<Robot> RobotPtr;
	typedef boost::weak_ptr<Robot> RobotWeakPtr;

	class RobotConfig;
	typedef boost::shared_ptr<RobotConfig> RobotConfigPtr;

	class RobotImporterFactory;
	typedef boost::shared_ptr<RobotImporterFactory> RobotImporterFactoryPtr;

	class RobotNode;
	typedef boost::shared_ptr<RobotNode> RobotNodePtr;
	typedef boost::weak_ptr<RobotNode> RobotNodeWeakPtr;

	class RobotNodeSet;
	typedef boost::shared_ptr<RobotNodeSet> RobotNodeSetPtr;

	class SceneObject;
	typedef boost::shared_ptr<SceneObject> SceneObjectPtr;
	typedef boost::weak_ptr<SceneObject> SceneObjectWeakPtr;

	class SceneObjectSet;
	typedef boost::shared_ptr<SceneObjectSet> SceneObjectSetPtr;

	class Obstacle;
	typedef boost::shared_ptr<Obstacle> ObstaclePtr;

	class KinematicChain;
	typedef boost::shared_ptr<KinematicChain> KinematicChainPtr;

	class Sensor;
	typedef boost::shared_ptr<Sensor> SensorPtr;

	class Trajectory;
	typedef boost::shared_ptr<Trajectory> TrajectoryPtr;

	class Visualization;
	typedef boost::shared_ptr<Visualization> VisualizationPtr;

	class VisualizationNode;
	typedef boost::shared_ptr<VisualizationNode> VisualizationNodePtr;

	class DifferentialIK;
	typedef boost::shared_ptr<DifferentialIK> DifferentialIKPtr;

	class SupportPolygon;
	typedef boost::shared_ptr<SupportPolygon> SupportPolygonPtr;

	class JacobiProvider;
	typedef boost::shared_ptr<JacobiProvider> JacobiProviderPtr;

	class IKSolver;
	typedef boost::shared_ptr<IKSolver> IKSolverPtr;

	class EndEffectorActor;
	typedef boost::shared_ptr<EndEffectorActor> EndEffectorActorPtr;

	class EndEffector;
	typedef boost::shared_ptr<EndEffector> EndEffectorPtr;

	/**************************************
	 ***          SceneObject           ***
	 **************************************/

	class SceneObject {
	public:
		enum VisualizationType {
			Full, Collision, CollisionData
		};

		// TODO: Nested struct currently ignored by SWIG
		struct Physics {
			enum CoMLocation {
				eCustom, eVisuBBoxCenter
			};

			enum SimulationType {
				eStatic, eKinematic, eDynamic, eUnknown
			};

			Physics();
			std::string getString(SimulationType s) const;
			void print() const;
			bool isSet();
			virtual std::string toXML(int tabs);
			Physics scale(float scaling) const;
			Eigen::Vector3f localCoM;
			float massKg;
			CoMLocation comLocation;
			Eigen::Matrix3f intertiaMatrix;
			SimulationType simType;
			std::vector< std::string > ignoreCollisions;
		};

		SceneObject(const std::string& name,
			VisualizationNodePtr visualization = VisualizationNodePtr(),
			CollisionModelPtr collisionModel = CollisionModelPtr(),
			const Physics& p = Physics(),
			CollisionCheckerPtr colChecker = CollisionCheckerPtr());
		virtual ~SceneObject();
		std::string getName() const;
		void setName(const std::string& name);
		virtual Eigen::Matrix4f getGlobalPose() const;
		virtual void setGlobalPoseNoChecks(const Eigen::Matrix4f& pose);
		virtual void setGlobalPose(const Eigen::Matrix4f& pose);
		virtual CollisionModelPtr getCollisionModel();
		virtual CollisionCheckerPtr getCollisionChecker();
		void setVisualization(VisualizationNodePtr visualization);
		void setCollisionModel(CollisionModelPtr colModel);
		virtual VisualizationNodePtr getVisualization(SceneObject::VisualizationType visuType = SceneObject::Full);
		virtual bool initialize(SceneObjectPtr parent = SceneObjectPtr(),
			const std::vector<SceneObjectPtr>& children = std::vector<SceneObjectPtr>());
		void setUpdateVisualization(bool enable);
		bool getUpdateVisualizationStatus();
		virtual void setupVisualization(bool showVisualization, bool showAttachedVisualizations);
		virtual void showCoordinateSystem(bool enable, float scaling = 1.0f, std::string* text = NULL);
		virtual void showPhysicsInformation(bool enableCoM, bool enableInertial,
			VisualizationNodePtr comModel = VisualizationNodePtr());
		virtual bool showCoordinateSystemState();
		virtual void showBoundingBox(bool enable, bool wireframe = false);
		virtual bool ensureVisualization(const std::string& visualizationType = "");
		Eigen::Matrix4f toLocalCoordinateSystem(const Eigen::Matrix4f& poseGlobal) const;
		Eigen::Vector3f toLocalCoordinateSystemVec(const Eigen::Vector3f& positionGlobal) const;
		Eigen::Matrix4f toGlobalCoordinateSystem(const Eigen::Matrix4f& poseLocal) const;
		Eigen::Vector3f toGlobalCoordinateSystemVec(const Eigen::Vector3f& positionLocal) const;
		Eigen::Matrix4f getTransformationTo(const SceneObjectPtr otherObject);
		Eigen::Matrix4f getTransformationFrom(const SceneObjectPtr otherObject);
		Eigen::Matrix4f transformTo(const SceneObjectPtr otherObject,
			const Eigen::Matrix4f& poseInOtherCoordSystem);
		Eigen::Vector3f transformTo(const SceneObjectPtr otherObject,
			const Eigen::Vector3f& positionInOtherCoordSystem);
		virtual int getNumFaces(bool collisionModel = false);
		virtual Eigen::Vector3f getCoMLocal();
		virtual Eigen::Vector3f getCoMGlobal();
		float getMass() const;
		void setMass(float m);
		Physics::SimulationType getSimulationType() const;
		void setSimulationType(Physics::SimulationType s);
		Eigen::Matrix3f getInertiaMatrix();
		void setInertiaMatrix(const Eigen::Matrix3f& im);
		std::vector<std::string> getIgnoredCollisionModels();
		virtual void print(bool printChildren = false, bool printDecoration = true) const;
		template <typename T> boost::shared_ptr<T> getVisualization(SceneObject::VisualizationType visuType = SceneObject::Full);
		void highlight(VisualizationPtr visualization, bool enable);
		SceneObjectPtr clone(const std::string& name,
			CollisionCheckerPtr colChecker = CollisionCheckerPtr(),
			float scaling = 1.0f) const;
		virtual bool attachChild(SceneObjectPtr child);
		virtual void detachChild(SceneObjectPtr child);
		virtual bool hasChild(SceneObjectPtr child, bool recursive = false) const;
		virtual bool hasChild(const std::string& childName, bool recursive = false) const;
		virtual bool hasParent();
		virtual SceneObjectPtr getParent() const;
		virtual std::vector<SceneObjectPtr> getChildren() const;
		virtual void updatePose(bool updateChildren = true);
		virtual bool saveModelFiles(const std::string& modelPath, bool replaceFilenames);

	};

	/**************************************
	 ***             Sensor             ***
	 **************************************/

	class Sensor : public SceneObject {
	public:
		RobotNodePtr getRobotNode() const;
		virtual Eigen::Matrix4f getRobotNodeToSensorTransformation();
		virtual void setGlobalPose(const Eigen::Matrix4f& pose);
		virtual void print(bool printChildren = false, bool printDecoration = true) const;
		virtual SensorPtr clone(RobotNodePtr newRobotNode, float scaling = 1.0f);
		SceneObjectPtr clone(const std::string& name,
			CollisionCheckerPtr colChecker = CollisionCheckerPtr(),
			float scaling = 1.0f) const;
		virtual void updatePose(bool updateChildren = true);
		virtual bool initialize(SceneObjectPtr parent = SceneObjectPtr(),
			const std::vector<SceneObjectPtr>& children = std::vector<SceneObjectPtr>());
		virtual std::string toXML(const std::string& modelPath, int tabs = 1);

	protected:
		/* Add a pure virtual method so that SWIG does not try to call the constructor (TODO: Maybe this can be removed later) */
		virtual SensorPtr _clone(const RobotNodePtr newRobotNode,
			const VisualizationNodePtr visualizationModel,
			float scaling) = 0;
	};

	class PositionSensor : public Sensor {
	public:
		PositionSensor(RobotNodeWeakPtr robotNode,
			const std::string& name,
			VisualizationNodePtr visualization = VisualizationNodePtr(),
			const Eigen::Matrix4f& rnTrafo = Eigen::Matrix4f::Identity());
		virtual void print(bool printChildren = false, bool printDecoration = true) const;
		virtual std::string toXML(const std::string& modelPath, int tabs);

	protected:
		virtual SensorPtr _clone(const RobotNodePtr newRobotNode,
			const VisualizationNodePtr visualizationModel,
			float scaling);
	};

	/**************************************
	 ***           RobotNode            ***
	 **************************************/

	class RobotNode : public SceneObject {
	public:
		enum RobotNodeType {
			Generic, Joint, Body, Transform
		};

		RobotPtr getRobot() const;
		void setJointValue(float q);
		void collectAllRobotNodes(std::vector< RobotNodePtr >& storeNodes);
		virtual float getJointValue() const;
		void respectJointLimits(float& jointValue) const;
		bool checkJointLimits(float jointValue, bool verbose = false) const;
		virtual Eigen::Matrix4f getLocalTransformation();
		virtual bool initialize(SceneObjectPtr parent = SceneObjectPtr(),
			const std::vector<SceneObjectPtr>& children = std::vector<SceneObjectPtr>());
		virtual void setGlobalPose(const Eigen::Matrix4f& pose);
		virtual Eigen::Matrix4f getGlobalPose() const;
		virtual Eigen::Matrix4f getPoseInRootFrame() const;
		virtual Eigen::Vector3f getPositionInRootFrame() const;
		virtual void showCoordinateSystem(bool enable,
			float scaling = 1.0f,
			std::string* text = NULL,
			const std::string& visualizationType = "");
		virtual void print(bool printChildren = false, bool printDecoration = true) const;
		float getJointLimitLo();
		float getJointLimitHi();
		virtual void setJointLimits(float lo, float hi);
		virtual bool isTranslationalJoint() const;
		virtual bool isRotationalJoint() const;
		virtual void showStructure(bool enable, const std::string& visualizationType = "");
		virtual std::vector<RobotNodePtr> getAllParents(RobotNodeSetPtr rns);
		virtual RobotNodePtr clone(RobotPtr newRobot,
			bool cloneChildren = true,
			RobotNodePtr initializeWithParent = RobotNodePtr(),
			CollisionCheckerPtr colChecker = CollisionCheckerPtr(),
			float scaling = 1.0f);
		float getJointValueOffset() const;
		float getJointLimitHigh() const;
		float getJointLimitLow() const;
		virtual void setMaxVelocity(float maxVel);
		virtual void setMaxAcceleration(float maxAcc);
		virtual void setMaxTorque(float maxTo);
		float getMaxVelocity();
		float getMaxAcceleration();
		float getMaxTorque();
		RobotNodeType getType();
		SceneObjectPtr clone(const std::string& name,
			CollisionCheckerPtr colChecker = CollisionCheckerPtr(),
			float scaling = 1.0f) const;
		const DHParameter& getOptionalDHParameters();
		virtual void updatePose(bool updateChildren = true);
		virtual void propagateJointValue(const std::string& jointName, float factor = 1.0f);
		virtual SensorPtr getSensor(const std::string& name) const;
		virtual bool hasSensor(const std::string& name) const;
		virtual std::vector<SensorPtr> getSensors() const;
		virtual bool registerSensor(SensorPtr sensor);
		virtual std::string toXML(const std::string& basePath,
			const std::string& modelPathRelative = "models",
			bool storeSensors = true);

	protected:
		/* Add a pure virtual method so that SWIG does not try to call the constructor (TODO: Maybe this can be removed later) */
		virtual std::string _toXML(const std::string& modelPath) = 0;
	};

	class RobotNodePrismatic : public RobotNode {
	public:
		RobotNodePrismatic(RobotWeakPtr rob,
			const std::string& name,
			float jointLimitLo,
			float jointLimitHi,
			const Eigen::Matrix4f& preJointTransform,
			const Eigen::Vector3f& translationDirection,
			VisualizationNodePtr visualization = VisualizationNodePtr(),
			CollisionModelPtr collisionModel = CollisionModelPtr(),
			float jointValueOffset = 0.0f,
			const SceneObject::Physics& p = SceneObject::Physics(),
			CollisionCheckerPtr colChecker = CollisionCheckerPtr(),
			RobotNodeType type = Generic);
		RobotNodePrismatic(RobotWeakPtr rob,
			const std::string& name,
			float jointLimitLo,
			float jointLimitHi,
			float a,
			float d,
			float alpha,
			float theta,
			VisualizationNodePtr visualization = VisualizationNodePtr(),
			CollisionModelPtr collisionModel = CollisionModelPtr(),
			float jointValueOffset = 0.0f,
			const SceneObject::Physics& p = SceneObject::Physics(),
			CollisionCheckerPtr colChecker = CollisionCheckerPtr(),
			RobotNodeType type = Generic);
		virtual bool initialize(SceneObjectPtr parent = SceneObjectPtr(),
			const std::vector<SceneObjectPtr>& children = std::vector<SceneObjectPtr>());
		virtual void print(bool printChildren = false, bool printDecoration = true) const;
		virtual bool isTranslationalJoint() const;
		Eigen::Vector3f getJointTranslationDirection(const SceneObjectPtr coordSystem = SceneObjectPtr()) const;
		Eigen::Vector3f getJointTranslationDirectionJointCoordSystem() const;
		void setVisuScaleFactor(Eigen::Vector3f& scaleFactor);
	};

	/**************************************
	 ***           SceneObjectSet       ***
	 **************************************/

	class SceneObjectSet {
	public:
		SceneObjectSet(const std::string& name = "", CollisionCheckerPtr colChecker = CollisionCheckerPtr());
		virtual ~SceneObjectSet();
		std::string getName() const;
		virtual bool addSceneObject(SceneObjectPtr sceneObject);
		virtual bool addSceneObjects(SceneObjectSetPtr sceneObjectSet);
		virtual bool addSceneObjects(RobotNodeSetPtr robotNodeSet);
		virtual bool addSceneObjects(std::vector<RobotNodePtr> robotNodes);
		virtual bool removeSceneObject(SceneObjectPtr sceneObject);
		CollisionCheckerPtr getCollisionChecker();
		std::vector< CollisionModelPtr > getCollisionModels();
		std::vector< SceneObjectPtr > getSceneObjects();
		virtual unsigned int getSize() const;
		virtual SceneObjectPtr getSceneObject(unsigned int nr);
		virtual bool hasSceneObject(SceneObjectPtr sceneObject);
		virtual bool getCurrentSceneObjectConfig(std::map< SceneObjectPtr, Eigen::Matrix4f >& storeConfig);
		virtual std::string toXML(int tabs);
		SceneObjectSetPtr clone(const std::string& newName = "");
		SceneObjectSetPtr clone(const std::string& newName, CollisionCheckerPtr newColChecker);
		ObstaclePtr createStaticObstacle(const std::string& name);
	protected:
		void destroyData();
	};

	/**************************************
	 ***           RobotNodeSet         ***
	 **************************************/

	class RobotNodeSet : public SceneObjectSet {
	public:
		static RobotNodeSetPtr createRobotNodeSet(RobotPtr robot,
			const std::string& name,
			const std::vector< std::string >& robotNodeNames,
			const std::string& kinematicRootName = "",
			const std::string& tcpName = "",
			bool registerToRobot = false);
		static RobotNodeSetPtr createRobotNodeSet(RobotPtr robot,
			const std::string& name,
			const std::vector< RobotNodePtr >& robotNodes,
			const RobotNodePtr kinematicRoot = RobotNodePtr(),
			const RobotNodePtr tcp = RobotNodePtr(),
			bool registerToRobot = false);
		RobotNodeSetPtr clone(RobotPtr newRobot, const RobotNodePtr newKinematicRoot = RobotNodePtr());
		bool hasRobotNode(RobotNodePtr robotNode) const;
		const std::vector< RobotNodePtr > getAllRobotNodes() const;
		RobotNodePtr getKinematicRoot() const;
		void setKinematicRoot(RobotNodePtr robotNode);
		RobotNodePtr getTCP() const;
		void print() const;
		virtual unsigned int getSize() const;
		std::vector<float> getJointValues() const;
		void getJointValues(std::vector<float>& fillVector) const;
		void getJointValues(Eigen::VectorXf& fillVector) const;
		void getJointValues(RobotConfigPtr fillVector) const;
		void respectJointLimits(std::vector<float>& jointValues) const;
		void respectJointLimits(Eigen::VectorXf& jointValues) const;
		bool checkJointLimits(std::vector<float>& jointValues, bool verbose = false) const;
		bool checkJointLimits(Eigen::VectorXf& jointValues, bool verbose = false) const;
		void setJointValues(const std::vector<float>& jointValues);
		void setJointValues(const Eigen::VectorXf& jointValues);
		void setJointValues(const RobotConfigPtr jointValues);
		RobotNodePtr& operator[](int i);
		RobotNodePtr& getNode(int i);
		RobotPtr getRobot();
		bool isKinematicChain();
		virtual void highlight(VisualizationPtr visualization, bool enable);
		virtual int getNumFaces(bool collisionModel = false);
		float getMaximumExtension();
		Eigen::Vector3f getCoM();
		float getMass();
		bool nodesSufficient(std::vector<RobotNodePtr> nodes) const;
		virtual std::string toXML(int tabs);
		virtual bool addSceneObject(SceneObjectPtr sceneObject);
		virtual bool addSceneObjects(SceneObjectSetPtr sceneObjectSet);
		virtual bool addSceneObjects(RobotNodeSetPtr robotNodeSet);
		virtual bool addSceneObjects(std::vector<RobotNodePtr> robotNodes);
		virtual bool removeSceneObject(SceneObjectPtr sceneObject);

	protected:
		bool isKinematicRoot(RobotNodePtr robotNode);
		RobotNodeSet(const std::string& name,
			RobotWeakPtr robot,
			const std::vector< RobotNodePtr >& robotNodes,
			const RobotNodePtr kinematicRoot = RobotNodePtr(),
			const RobotNodePtr tcp = RobotNodePtr());
	};

	/**************************************
	 ***       RobotNodeRevolute        ***
	 **************************************/

	class RobotNodeRevolute : public RobotNode {
	public:
		RobotNodeRevolute(RobotWeakPtr rob,
			const std::string& name,
			float jointLimitLo,
			float jointLimitHi,
			const Eigen::Matrix4f& preJointTransform,
			const Eigen::Vector3f& axis,
			VisualizationNodePtr visualization = VisualizationNodePtr(),
			CollisionModelPtr collisionModel = CollisionModelPtr(),
			float jointValueOffset = 0.0f,
			const SceneObject::Physics& p = SceneObject::Physics(),
			CollisionCheckerPtr colChecker = CollisionCheckerPtr(),
			RobotNodeType type = Generic);
		RobotNodeRevolute(RobotWeakPtr rob,
			const std::string& name,
			float jointLimitLo,
			float jointLimitHi,
			float a,
			float d,
			float alpha,
			float theta,
			VisualizationNodePtr visualization = VisualizationNodePtr(),
			CollisionModelPtr collisionModel = CollisionModelPtr(),
			float jointValueOffset = 0.0f,
			const SceneObject::Physics& p = SceneObject::Physics(),
			CollisionCheckerPtr colChecker = CollisionCheckerPtr(),
			RobotNodeType type = Generic);
		virtual bool initialize(SceneObjectPtr parent = SceneObjectPtr(),
			const std::vector<SceneObjectPtr>& children = std::vector<SceneObjectPtr>());
		virtual void print(bool printChildren = false, bool printDecoration = true) const;
		virtual bool isRotationalJoint() const;
		Eigen::Vector3f getJointRotationAxis(const SceneObjectPtr coordSystem = SceneObjectPtr()) const;
		Eigen::Vector3f getJointRotationAxisInJointCoordSystem() const;
		virtual float getLMTC(float angle);
		virtual float getLMomentArm(float angle);
	};

	/**************************************
	 ***             Robot              ***
	 **************************************/

	class Robot : public SceneObject {
	public:

		Robot(const std::string& name, const std::string& type = "");
		virtual void setRootNode(RobotNodePtr node) = 0;
		virtual RobotNodePtr getRootNode() = 0;
		virtual void applyJointValues();
		virtual void setThreadsafe(bool);
		template <typename T> boost::shared_ptr<T> getVisualization(SceneObject::VisualizationType visuType = SceneObject::Full,
			bool sensors = true);
		void showStructure(bool enable, const std::string& type = "");
		void showCoordinateSystems(bool enable, const std::string& type = "");
		void showPhysicsInformation(bool enableCoM,
			bool enableInertial,
			VisualizationNodePtr comModel = VisualizationNodePtr());
		void setupVisualization(bool showVisualization, bool showAttachedVisualizations);
		virtual std::string getName();
		virtual std::string getType();
		virtual void print();
		void setUpdateVisualization(bool enable);
		bool getUpdateVisualizationStatus();
		boost::shared_ptr<Robot> shared_from_this();
		virtual RobotConfigPtr getConfig();
		virtual bool setConfig(RobotConfigPtr c);
		virtual void registerRobotNode(RobotNodePtr node) = 0;
		virtual void deregisterRobotNode(RobotNodePtr node) = 0;
		virtual bool hasRobotNode(RobotNodePtr node) = 0;
		virtual bool hasRobotNode(const std::string& robotNodeName) = 0;
		virtual RobotNodePtr getRobotNode(const std::string& robotNodeName) = 0;
		virtual std::vector< RobotNodePtr > getRobotNodes();
		virtual void getRobotNodes(std::vector< RobotNodePtr >& storeNodes, bool clearVector = true) = 0;
		virtual void registerRobotNodeSet(RobotNodeSetPtr nodeSet) = 0;
		virtual void deregisterRobotNodeSet(RobotNodeSetPtr nodeSet) = 0;
		virtual bool hasRobotNodeSet(RobotNodeSetPtr nodeSet);
		virtual bool hasRobotNodeSet(const std::string& name) = 0;
		virtual RobotNodeSetPtr getRobotNodeSet(const std::string& nodeSetName) = 0;
		virtual std::vector<RobotNodeSetPtr> getRobotNodeSets();
		virtual void getRobotNodeSets(std::vector<RobotNodeSetPtr>& storeNodeSet) = 0;
		virtual void registerEndEffector(EndEffectorPtr endEffector) = 0;
		virtual bool hasEndEffector(EndEffectorPtr endEffector);
		virtual bool hasEndEffector(const std::string& endEffectorName) = 0;
		virtual EndEffectorPtr getEndEffector(const std::string& endEffectorName) = 0;
		virtual std::vector<EndEffectorPtr> getEndEffectors();
		virtual void getEndEffectors(std::vector<EndEffectorPtr>& storeEEF) = 0;
		virtual std::vector<CollisionModelPtr> getCollisionModels();
		virtual CollisionCheckerPtr getCollisionChecker();
		virtual void highlight(VisualizationPtr visualization, bool enable);
		virtual int getNumFaces(bool collisionModel = false);
		virtual void setGlobalPose(const Eigen::Matrix4f& globalPose, bool applyValues) = 0;  // TODO: Removed "= true"
		virtual void setGlobalPoseForRobotNode(const RobotNodePtr& node, const Eigen::Matrix4f& globalPoseNode);
		virtual Eigen::Vector3f getCoMLocal();
		virtual Eigen::Vector3f getCoMGlobal();
		virtual float getMass();
		virtual RobotPtr extractSubPart(RobotNodePtr startJoint,
			const std::string& newRobotType,
			const std::string& newRobotName,
			bool cloneRNS = true,
			bool cloneEEFs = true,
			CollisionCheckerPtr collisionChecker = CollisionCheckerPtr(),
			float scaling = 1.0f);
		virtual RobotPtr clone(const std::string& name,
			CollisionCheckerPtr collisionChecker = CollisionCheckerPtr(),
			float scaling = 1.0f);
		virtual void setFilename(const std::string& filename);
		virtual std::string getFilename();
		virtual ReadLockPtr getReadLock();
		virtual WriteLockPtr getWriteLock();
		virtual void setJointValue(RobotNodePtr rn, float jointValue);
		virtual void setJointValue(const std::string& nodeName, float jointValue);
		virtual void setJointValues(const std::map< std::string, float >& jointValues);
		virtual void setJointValues(const std::map< RobotNodePtr, float >& jointValues);
		virtual void setJointValues(RobotNodeSetPtr rns, const std::vector<float>& jointValues);
		virtual void setJointValues(const std::vector<RobotNodePtr> rn, const std::vector<float>& jointValues);
		virtual void setJointValues(RobotNodeSetPtr rns, const Eigen::VectorXf& jointValues);
		virtual void setJointValues(RobotConfigPtr config);
		virtual void setJointValues(RobotNodeSetPtr rns, RobotConfigPtr config);
		virtual void setJointValues(TrajectoryPtr trajectory, float t);
		virtual BoundingBox getBoundingBox(bool collisionModel = true);
		virtual SensorPtr getSensor(const std::string& name);
		virtual std::vector<SensorPtr> getSensors();
		virtual std::string toXML(const std::string& basePath = ".",
			const std::string& modelPath = "models",
			bool storeEEF = true,
			bool storeRNS = true,
			bool storeSensors = true);
		virtual std::vector<Eigen::Vector2f> getSupportPolygon();
		virtual std::vector<Eigen::Vector2f> getProjectedFootPolygon();
		float getScaling();
		std::string getcolModelNames();
		std::string checkStanceFoot();
	};

	/**************************************
	 ***           LocalRobot           ***
	 **************************************/

	class LocalRobot : public Robot {
	public:
		LocalRobot(const std::string& name, const std::string& type = "");
		virtual void setRootNode(RobotNodePtr node);
		virtual RobotNodePtr getRootNode();
		virtual void registerRobotNode(RobotNodePtr node);
		virtual void deregisterRobotNode(RobotNodePtr node);
		virtual bool hasRobotNode(const std::string& robotNodeName);
		virtual bool hasRobotNode(RobotNodePtr node);
		virtual RobotNodePtr getRobotNode(const std::string& robotNodeName);
		virtual void getRobotNodes(std::vector< RobotNodePtr >& storeNodes, bool clearVector = true);
		virtual void registerRobotNodeSet(RobotNodeSetPtr nodeSet);
		virtual void deregisterRobotNodeSet(RobotNodeSetPtr nodeSet);
		virtual bool hasRobotNodeSet(const std::string& name);
		virtual RobotNodeSetPtr getRobotNodeSet(const std::string& nodeSetName);
		virtual void getRobotNodeSets(std::vector<RobotNodeSetPtr>& storeNodeSet);
		virtual void registerEndEffector(EndEffectorPtr endEffector);
		virtual bool hasEndEffector(const std::string& endEffectorName);
		virtual EndEffectorPtr getEndEffector(const std::string& endEffectorName);
		virtual void getEndEffectors(std::vector<EndEffectorPtr>& storeEEF);
		virtual void setGlobalPose(const Eigen::Matrix4f& globalPose, bool applyJointValues);  // TODO: Removed "= true"
		// virtual void setGlobalPose(const Eigen::Matrix4f& globalPose);
		virtual Eigen::Matrix4f getGlobalPose();
	};

	/**************************************
	 ***          RobotConfig           ***
	 **************************************/

	class RobotConfig {
	public:
		struct Configuration {
			std::string name;
			float value;
		};

		RobotConfig(RobotWeakPtr robot, const std::string& name);
		RobotConfig(RobotWeakPtr robot, const std::string& name, const std::map< RobotNodePtr, float >& configs);
		RobotConfig(RobotWeakPtr robot, const std::string& name, const std::vector< Configuration >& configs);
		RobotConfig(RobotWeakPtr robot,
			const std::string& name,
			const std::vector< std::string >& robotNodes,
			const std::vector< float >& values);
		RobotConfig(RobotWeakPtr robot,
			const std::string& name,
			const std::vector< RobotNodePtr >& robotNodes,
			const std::vector< float >& values);
		RobotConfigPtr clone(RobotPtr newRobot = RobotPtr());
		std::string getName() const;
		void print() const;
		RobotPtr getRobot();
		bool setConfig(const Configuration& c);
		bool setConfig(RobotNodePtr node, float value);
		bool setConfig(const std::string& node, float value);
		bool setJointValues();
		bool setJointValues(RobotPtr r);
		bool hasConfig(const std::string& name) const;
		float getConfig(const std::string& name) const;
		std::vector< RobotNodePtr > getNodes() const;
		std::map < std::string, float > getRobotNodeJointValueMap();
		std::string toXML(int tabs = 0);
		static std::string createXMLString(const std::map< std::string,
			float >& config,
			const std::string& name,
			int tabs = 0);
	};

	/**************************************
	 ***            RobotIO             ***
	 **************************************/

	class RobotIO {
	public:
		enum RobotDescription {
			eFull, eCollisionModel, eStructure
		};

	protected:
		RobotIO();
		virtual ~RobotIO();
	};

	/**************************************
	 ***      RobotImporterFactory      ***
	 **************************************/

	class RobotImporterFactory {
	public:
		RobotImporterFactory();
		virtual ~RobotImporterFactory();
		virtual RobotPtr loadFromFile(const std::string& filename, RobotIO::RobotDescription loadMode = RobotIO::eFull) = 0;
		virtual std::string getFileExtension() = 0;
		virtual std::string getFileFilter() = 0;
		static std::string getAllFileFilters();
		static std::string getAllExtensions();
		static RobotImporterFactoryPtr fromFileExtension(std::string type, void* params);
	};

	class SimoxXMLFactory : public RobotImporterFactory {
	public:
		SimoxXMLFactory();
		virtual ~SimoxXMLFactory();
		virtual RobotPtr loadFromFile(const std::string& filename, RobotIO::RobotDescription loadMode = RobotIO::eFull);
		static std::string getName();
		static boost::shared_ptr<RobotImporterFactory> createInstance(void*);
		virtual std::string getFileFilter();
		virtual std::string getFileExtension();
	};

	/**************************************
	 ***         DifferentialIK         ***
	 **************************************/

	class DifferentialIK {
	public:
		DifferentialIK(RobotNodeSetPtr rns, RobotNodePtr coordSystem = RobotNodePtr(), JacobiProvider::InverseJacobiMethod invJacMethod = eSVD, float invParam = 0.0f);
		virtual Eigen::MatrixXf getJacobianMatrix(SceneObjectPtr tcp, IKSolver::CartesianSelection mode);
		virtual Eigen::MatrixXf getJacobianMatrix(SceneObjectPtr tcp);
		virtual Eigen::MatrixXf getJacobianMatrix(IKSolver::CartesianSelection mode);
		virtual Eigen::MatrixXf getJacobianMatrix();
	};
	
	/**************************************
	 ***        MathTools               ***
	**************************************/
	//class MathTools{
		

	//};
	
	/******************************************
	** 		CollisionModel		**
	*****************************************/

	class CollisionModel{
	public:
        	CollisionModel(const VisualizationNodePtr visu, const std::string& name = "", CollisionCheckerPtr colChecker = CollisionCheckerPtr(), int id = 666);

		Eigen::Matrix4f getGlobalPose();
	};


	/**************************************
	***          SupportPolygon         ***
	***************************************/
	class SupportPolygon {
	public:
		SupportPolygon(SceneObjectSetPtr contactModels);
		std::vector< Eigen::Vector2f > currentContactPoints2D;

		//MathTools::ConvexHull2DPtr getSupportPolygon2D();

	};



	/**************************************
	 ***          EndEffector           ***
	 **************************************/
	class EndEffector {
	public:
		EndEffector(const std::string& nameString, const std::vector<EndEffectorActorPtr>& actorsVector, const std::vector<RobotNodePtr>& staticPartVector, RobotNodePtr baseNodePtr, RobotNodePtr tcpNodePtr, RobotNodePtr gcpNodePtr = RobotNodePtr(), std::vector< RobotConfigPtr > preshapes = std::vector< RobotConfigPtr >());
		virtual ~EndEffector();
		EndEffectorPtr clone(RobotPtr newRobot);
		std::string getName();
		std::string getBaseNodeName();
		std::string getTcpName();
		RobotNodePtr getTcp();
		RobotNodePtr getBase();
		RobotNodePtr getGCP();
		RobotPtr getRobot();
	};
}

/**************************************
 ***  Helpers for shared_ptr casts  ***
 **************************************/

// TODO: Maybe there is a better way to do this
%inline %{
	namespace VirtualRobot {
		class PointerDowncasts {
		public:
			static LocalRobotPtr RobotToLocalRobot(RobotPtr base) { return boost::dynamic_pointer_cast<LocalRobot>(base); }
		};
	}
%}
